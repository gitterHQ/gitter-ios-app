import Foundation
import CoreData

class GitterTabBarController: UITabBarController, UITabBarControllerDelegate {

    private let notificationCenter = NotificationCenter.default
    private let managedObjectContext = CoreDataSingleton.sharedInstance.uiManagedObjectContext
    private var previousViewController: UIViewController?
    private let unreadColor = Colors.caribbeanColor()
    private let mentionColor = Colors.jaffaColor()

    private var model = Model() {
        didSet {
            render()
        }
    }

    override func loadView() {
        super.loadView()

        UITabBarItem.appearance().setTitleTextAttributes(convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.foregroundColor): Colors.rubyColor()]), for: .selected)
        UITabBar.appearance().tintColor = Colors.rubyColor()

        delegate = self

        notificationCenter.addObserver(self, selector: #selector(self.managedObjectContextDidSave), name: NSNotification.Name.NSManagedObjectContextDidSave, object: managedObjectContext)

        // we would normally do a network request for the room list, but the live room collection should do that for us.

        query({ (model) in
            self.model = model
        })
    }

    deinit {
        notificationCenter.removeObserver(self)
    }

    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        previousViewController = selectedViewController
        return true
    }

    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        guard viewController == previousViewController else {
            // ensures that we dont always scroll to the top when switching tabs
            return
        }

        scrollToTopOfVisibleTableView()
    }

    @objc func managedObjectContextDidSave(_ notification: Notification) {
        let userInfo = (notification as NSNotification).userInfo!
        let insertedObjects = userInfo[NSInsertedObjectsKey] as? Set<NSManagedObject> ?? []
        let updatedObjects = userInfo[NSUpdatedObjectsKey] as? Set<NSManagedObject> ?? []
        let deletedObjects = userInfo[NSDeletedObjectsKey] as? Set<NSManagedObject> ?? []

        if isRequeryRequired(insertedObjects: insertedObjects, updatedObjects: updatedObjects, deletedObjects: deletedObjects) {
            query({ (model) in
                self.model = model
            })
        }
    }

    func switchTabs(_ tab: GitterTab) {
        selectedViewController = viewControllers![tab.rawValue]
    }

    private func scrollToTopOfVisibleTableView() {
        if let visibleTableViewController = (selectedViewController as? UINavigationController)?.visibleViewController as? UITableViewController {
            let tableView = visibleTableViewController.tableView
            tableView?.setContentOffset(CGPoint(x: 0, y: -tableView!.contentInset.top), animated:true)
        }
    }

    private func query(_ completionHandler: @escaping (Model) -> Void) {
        let fetchRequest = NSFetchRequest<Room>(entityName: "Room")
        // we only go as far as 99+ for unread labels, so limit to 100 (the communities badge could be underestimated though)
        fetchRequest.fetchLimit = 100
        fetchRequest.predicate = NSPredicate(format: "userRoomCollection != NULL AND (mentions > 0 OR unreadItems > 0)")

        try! managedObjectContext.execute(NSAsynchronousFetchRequest(fetchRequest: fetchRequest, completionBlock: { (result) in

            let rooms = result.finalResult!
            var unreadRooms = 0
            var mentionedRooms = 0
            var unreadOneToOnes = 0
            var mentionedOneToOnes = 0
            var unreadGroups = Set<String>()
            var mentionedGroups = Set<String>()

            // we're still in the background thread, so its a great time to do some processing
            rooms.forEach({ (room) in
                if (room.intUnreadItems > 0) {
                    unreadRooms += 1
                    if (room.oneToOne) {
                        unreadOneToOnes += 1
                    }
                    if let groupId = room.groupId {
                        unreadGroups.insert(groupId)
                    }
                }
                if (room.intMentions > 0) {
                    mentionedRooms += 1
                    if (room.oneToOne) {
                        mentionedOneToOnes += 1
                    }
                    if let groupId = room.groupId {
                        mentionedGroups.insert(groupId)
                    }
                }
            })

            DispatchQueue.main.async(execute: {
                // back in the main thread
                let model = Model()
                model.unreadRooms = unreadRooms
                model.mentionedRooms = mentionedRooms
                model.unreadOneToOnes = unreadOneToOnes
                model.mentionedOneToOnes = mentionedOneToOnes
                model.unreadGroups = unreadGroups.count
                model.mentionedGroups = mentionedGroups.count

                completionHandler(model)
            })
        }))
    }

    private func render() {
        viewControllers?[GitterTab.home.rawValue].tabBarItem.badgeValue = model.roomsLabelText
        viewControllers?[GitterTab.people.rawValue].tabBarItem.badgeValue = model.oneToOnesLabelText
        viewControllers?[GitterTab.communities.rawValue].tabBarItem.badgeValue = model.groupsLabelText

        // this is could break on ios update, but this code is super brittle and so would just fail back to the old tab label colours
        findBadges(view) { (badgeText, badgeBackground, oldGitterBadgeBackground) in
            if let badgeText = badgeText , badgeText == "@" || Int(badgeText) != nil {
                if let oldGitterBadgeBackground = oldGitterBadgeBackground {
                    oldGitterBadgeBackground.removeFromSuperview()
                }

                let superview = badgeBackground.superview!

                let gitterBadgeBackground = BadgeView()
                gitterBadgeBackground.translatesAutoresizingMaskIntoConstraints = false
                superview.insertSubview(gitterBadgeBackground, aboveSubview: badgeBackground)

                let horizontalConstraint = gitterBadgeBackground.centerXAnchor.constraint(equalTo: superview.centerXAnchor)
                let verticalConstraint = gitterBadgeBackground.centerYAnchor.constraint(equalTo: superview.centerYAnchor)
                // Because of the (unremovable!) existing badge behind it, there are some aliasing artifacts behind the new badge.
                // The solution is to make it just a bit bigger than the one it covers
                let widthConstraint = gitterBadgeBackground.widthAnchor.constraint(equalTo: superview.widthAnchor, constant: 1.1)
                let heightConstraint = gitterBadgeBackground.heightAnchor.constraint(equalTo: superview.heightAnchor, constant: 1.1)
                NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])

                gitterBadgeBackground.layer.masksToBounds = true
                gitterBadgeBackground.backgroundColor = badgeText == "@" ? self.mentionColor : self.unreadColor
            }
        }

        // ideally this should be in a different class as it doesnt belong to the tab bar,
        // but we've already got the badge number here in a live and async way. Plus, this vc is pretty global
        UIApplication.shared.applicationIconBadgeNumber = model.unreadRooms
    }

    private func isRequeryRequired(insertedObjects: Set<NSManagedObject>, updatedObjects: Set<NSManagedObject>, deletedObjects: Set<NSManagedObject>) -> Bool {
        return insertedObjects.union(updatedObjects).union(deletedObjects).contains(where: { (managedObject) -> Bool in
            // we can technically get finer grained than this (e.g ignore room inserts that are not in a group),
            // but its better to be simple and slightly too eager. Stuck unread badges suck.
            return managedObject.entity.name == "Room"
        })
    }

    private func findBadges(_ view: UIView, badgeFoundHandler: (_ badgeText: String?, _ badgeBackground: UIView, _ gitterBadgeBackground: UIView?) -> Void) {
        let subviews = view.subviews
        if let label = subviews.last as? UILabel , subviews.count == 2 || subviews.count == 3 {
            if (subviews.count == 2) {
                badgeFoundHandler(label.text, subviews[0], nil)
            } else {
                badgeFoundHandler(label.text, subviews[0], subviews[1])
            }
        } else {
            subviews.forEach({ (view) in
                findBadges(view, badgeFoundHandler: badgeFoundHandler)
            })
        }
    }

    private class Model {
        var unreadRooms = 0
        var mentionedRooms = 0
        var unreadOneToOnes = 0
        var mentionedOneToOnes = 0
        var unreadGroups = 0
        var mentionedGroups = 0

        var roomsLabelText: String? {
            get {
                return labelText(unreads: unreadRooms, mentions: mentionedRooms)
            }
        }
        var oneToOnesLabelText: String? {
            get {
                return labelText(unreads: unreadOneToOnes, mentions: mentionedOneToOnes)
            }
        }
        var groupsLabelText: String? {
            get {
                return labelText(unreads: unreadGroups, mentions: mentionedGroups)
            }
        }

        private func labelText(unreads: Int, mentions: Int) -> String? {
            if mentions > 0 {
                return "@"
            } else if unreads > 0 {
                return unreads > 99 ? "99+" : String(unreads)
            } else {
                return nil
            }
        }
    }

    private class BadgeView: UIView {
        override func layoutSubviews() {
            super.layoutSubviews()
            layoutIfNeeded()
            self.layer.cornerRadius = self.frame.size.height / 2;
        }
    }
}

enum GitterTab: Int {
    case home = 0
    case search = 1
    case people = 2
    case communities = 3
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}
