import Foundation

class RestService {

    private let authSession = Api().authSession()
    private let jsonToDb = JsonToDatabaseHelper()
    private let api = Api()
    typealias RestCallback = (_ error: Error?, _ didSaveNewData: Bool) -> Void
    typealias RestCallbackWithId = (_ error: Error?, _ id: String?) -> Void
    typealias RestCallbackWithIds = (_ error: Error?, _ ids: [String]?) -> Void

    func requestRoom(_ id: String, completionHandler: @escaping RestCallback) {
        self.authSession.dataTask(with: Api().urlWithPath("/v1/rooms/\(id)"), completionHandler: api.parseJsonObject({ (error, json) in
            guard let json = json , error == nil else {
                return completionHandler(error, false)
            }

            self.jsonToDb.upsertRoom(json, completionHandler: completionHandler)
        })).resume()
    }

    func createRoom(_ body: JsonObject, inGroup group: Group, completionHandler: @escaping RestCallbackWithId) {
        var request = Api().postRequest("/v1/groups/\(group.id)/rooms")
        request.httpBody = try! JSONSerialization.data(withJSONObject: body, options: [])

        authSession.dataTask(with: request, completionHandler: api.parseJsonObject({ (error, json) in
            guard let json = json , error == nil else {
                return completionHandler(error, nil)
            }

            self.jsonToDb.upsertRoom(json, completionHandler: { (error, didSaveNewData) in
                completionHandler(error, json["id"] as? String)
            })
        })).resume()
    }

    func findOrCreateRoom(_ uri: String, completionHandler: @escaping RestCallbackWithId) {
        var request = Api().postRequest("/v1/rooms")
        request.httpBody = try! JSONSerialization.data(withJSONObject: ["uri": uri], options: [])

        authSession.dataTask(with: request, completionHandler: api.parseJsonObject({ (error, json) in
            guard let json = json , error == nil else {
                return completionHandler(error, nil)
            }

            self.jsonToDb.upsertRoom(json, completionHandler: { (error, didSaveNewData) in
                completionHandler(error, json["id"] as? String)
            })
        })).resume()
    }

    func searchRooms(_ query: String, completionHandler: @escaping RestCallbackWithIds) {
        var searchUrl = URLComponents(url: api.urlWithPath("/v1/rooms"), resolvingAgainstBaseURL: true)!
        searchUrl.query = "q=\(query)"

        authSession.dataTask(with: searchUrl.url!, completionHandler: api.parseJsonObject({ (error, json) in
            guard let results = json?["results"] as? [JsonObject] , error == nil else {
                return completionHandler(error, nil)
            }

            self.jsonToDb.upsertRooms(results, completionHandler: { (error, didSaveNewData) in
                guard error == nil else {
                    return completionHandler(error!, nil)
                }

                let ids = results.map({ (room) -> String in
                    return room["id"] as! String
                })

                completionHandler(nil, ids)
            })
        })).resume()
    }

    func joinRoom(_ roomId: String, completionHandler: @escaping RestCallback) {
        var request = Api().postRequest("/v1/user/me/rooms")
        request.httpBody = try! JSONSerialization.data(withJSONObject: ["id": roomId], options: [])

        authSession.dataTask(with: request, completionHandler: api.parseJsonObject({ (error, json) in
            guard let json = json , error == nil else {
                return completionHandler(error, false)
            }

            self.jsonToDb.upsertRoom(json, completionHandler: { (error, didSaveNewData) in
                completionHandler(error, didSaveNewData)
            })
        })).resume()
    }

    func leaveRoom(_ roomId: String, completionHandler: @escaping RestCallback) {
        let userId = LoginData().getUserId()!
        let request = Api().deleteRequest("/v1/rooms/\(roomId)/users/\(userId)")
        self.authSession.dataTask(with: request, completionHandler: api.assertErrorless({ (error) -> Void in
            guard error == nil else {
                return completionHandler(error, false)
            }

            self.requestRoom(roomId, completionHandler: completionHandler)
        })).resume()
    }

    func markAllAsRead(_ roomId: String, completionHandler: @escaping RestCallback) {
        let request = Api().deleteRequest("/v1/user/me/rooms/\(roomId)/unreadItems/all")

        self.authSession.dataTask(with: request, completionHandler: api.assertErrorless({ (error) -> Void in
            completionHandler(error, false)
        })).resume()
    }

    func getUsers(inRoom roomId: String, limit: Int = 10, completionHandler: @escaping RestCallbackWithIds) {
        var searchUrl = URLComponents(url: api.urlWithPath("/v1/rooms/\(roomId)/users"), resolvingAgainstBaseURL: true)!
        searchUrl.queryItems = [URLQueryItem(name: "limit", value: String(limit))]

        authSession.dataTask(with: searchUrl.url!, completionHandler: api.parseJsonArray({ (error, json) in
            guard let json = json , error == nil else {
                return completionHandler(error, nil)
            }

            self.jsonToDb.upsertUsers(json, completionHandler: { (error, didSaveNewData) in
                guard error == nil else {
                    return completionHandler(error!, nil)
                }

                let ids = json.map({ (room) -> String in
                    return room["id"] as! String
                })

                completionHandler(nil, ids)
            })
        })).resume()
    }

    func searchUsers(_ query: String, limit: Int = 10, completionHandler: @escaping RestCallbackWithIds) {
        var searchUrl = URLComponents(url: api.urlWithPath("/v1/user"), resolvingAgainstBaseURL: true)!
        searchUrl.queryItems = [
            URLQueryItem(name: "q", value: query),
            URLQueryItem(name: "type", value: "gitter"),
            URLQueryItem(name: "limit", value: String(limit))
        ]

        authSession.dataTask(with: searchUrl.url!, completionHandler: api.parseJsonObject({ (error, json) in
            guard let results = json?["results"] as? [JsonObject] , error == nil else {
                return completionHandler(error, nil)
            }

            self.jsonToDb.upsertUsers(results, completionHandler: { (error, didSaveNewData) in
                guard error == nil else {
                    return completionHandler(error!, nil)
                }

                let ids = results.map({ (room) -> String in
                    return room["id"] as! String
                })

                completionHandler(nil, ids)
            })
        })).resume()
    }

    func searchUsers(inRoom roomId: String, query: String, limit: Int = 10, completionHandler: @escaping RestCallbackWithIds) {
        var searchUrl = URLComponents(url: api.urlWithPath("/v1/rooms/\(roomId)/users"), resolvingAgainstBaseURL: true)!
        searchUrl.queryItems = [
            URLQueryItem(name: "q", value: query),
            URLQueryItem(name: "type", value: "gitter"),
            URLQueryItem(name: "limit", value: String(limit))
        ]

        authSession.dataTask(with: searchUrl.url!, completionHandler: api.parseJsonArray({ (error, json) in
            guard let json = json , error == nil else {
                return completionHandler(error, nil)
            }

            self.jsonToDb.upsertUsers(json, completionHandler: { (error, didSaveNewData) in
                guard error == nil else {
                    return completionHandler(error!, nil)
                }

                let ids = json.map({ (room) -> String in
                    return room["id"] as! String
                })

                completionHandler(nil, ids)
            })
        })).resume()
    }

    func getUserRooms(_ completionHandler: @escaping RestCallback) {
        authSession.dataTask(with: api.urlWithPath("/v1/user/me/rooms"), completionHandler: api.parseJsonArray({ (error, jsonArray) in
            guard let jsonArray = jsonArray , error == nil else {
                return completionHandler(error, false)
            }

            self.jsonToDb.updateUserRoomList(jsonArray, completionHandler: completionHandler)
        })).resume()
    }

    func getSuggestions(_ completionHandler: @escaping RestCallback) {
        authSession.dataTask(with: api.urlWithPath("/v1/user/me/suggestedRooms"), completionHandler: api.parseJsonArray({ (error, jsonArray) in
            guard let jsonArray = jsonArray , error == nil else {
                return completionHandler(error, false)
            }

            self.jsonToDb.updateSuggestions(jsonArray, completionHandler: completionHandler)
        })).resume()
    }

    func getGroup(byId id: String, completionHandler: @escaping RestCallback) {
        authSession.dataTask(with: api.urlWithPath("/v1/groups/\(id)"), completionHandler: api.parseJsonObject({ (error, json) in
            guard let json = json , error == nil else {
                return completionHandler(error, false)
            }

            self.jsonToDb.upsertGroup(json, completionHandler: completionHandler)
        })).resume()
    }

    func getAdminGroups(_ completionHandler: @escaping RestCallback) {
        authSession.dataTask(with: api.urlWithPath("/v1/groups?type=admin"), completionHandler: api.parseJsonArray({ (error, jsonArray) in
            guard let jsonArray = jsonArray , error == nil else {
                return completionHandler(error, false)
            }

            self.jsonToDb.updateAdminGroupList(jsonArray, completionHandler: completionHandler)
        })).resume()
    }

    func getRooms(forGroupId groupId: String, completionHandler: @escaping RestCallback) {
        authSession.dataTask(with: api.urlWithPath("/v1/groups/\(groupId)/rooms"), completionHandler: api.parseJsonArray({ (error, jsonArray) in
            guard let jsonArray = jsonArray , error == nil else {
                return completionHandler(error, false)
            }

            self.jsonToDb.updateRoomList(jsonArray, forGroupId: groupId, completionHandler: completionHandler)
        })).resume()
    }

    func getUserGroups(_ completionHandler: @escaping RestCallback) {
        authSession.dataTask(with: api.urlWithPath("/v1/user/me/groups"), completionHandler: api.parseJsonArray({ (error, jsonArray) in
            guard let jsonArray = jsonArray , error == nil else {
                return completionHandler(error, false)
            }

            self.jsonToDb.updateUserGroupList(jsonArray, completionHandler: completionHandler)
        })).resume()
    }

    func send(message: String, toRoomId roomId: String, completionHandler: @escaping RestCallback) {
        var request = Api().postRequest("/v1/rooms/\(roomId)/chatMessages")
        request.httpBody = try! JSONSerialization.data(withJSONObject: ["text": message], options: [])

        authSession.dataTask(with: request, completionHandler: api.assertErrorless({ (error) in
            completionHandler(error, false)
        })).resume()
    }
}
